# Preview

![Resultado final](screen1.png)
![Resultado final](screen2.png)

## 🛠️ Instalación

```
npm install
```

## 🏃 Ejecución

```
npm run start
```

### ⚗️ Pruebas Unitarias

```
npm run test
```
