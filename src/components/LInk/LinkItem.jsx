import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';
import './linkItem.css';

const LinkItem = ({ name, url }) => {
  const variants = {
    hidden: { opacity: 0, scaleX: 0 },
    show: { opacity: 1, scaleX: [0.4, 1.1, 1], transition: 1 },
  };

  return (
    <motion.li variants={variants}>
      <a href={url} target="_blank" rel="noreferrer" className="a_link_profile">
        <i className={`bx bxl-${name.toLowerCase()} i_link_profile`}></i>
        <span>{name}</span>
      </a>
    </motion.li>
  );
};

LinkItem.propTypes = {
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default memo(LinkItem);
