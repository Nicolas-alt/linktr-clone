import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import App from '../App/App';
import Profile from '../../pages/Profile/Profile';

describe('Test on <App />', () => {
  test('Should take a snapshot ', () => {
    const { container } = render(
      <App>
        <Profile />
      </App>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
