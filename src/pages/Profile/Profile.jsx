import React, { useContext } from 'react';
import Footer from '../../components/Footer/Footer';
import LinkItem from '../../components/LInk/LinkItem';
import Spinner from '../../components/Spinner/Spinner';
import { UserContext } from '../../providers/UserProvider';
import { motion } from 'framer-motion';
import './profile.css';

const Profile = () => {
  const {
    coors,
    setCoors,
    userData: {
      data: { avatar, name, profession, social },
      loading,
    },
  } = useContext(UserContext);

  const variants = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.5,
      },
    },
  };

  const container = {
    hidden: { opacity: 1, scale: 0 },
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        delayChildren: 0.8,
        staggerChildren: 0.6,
      },
    },
  };

  const item = {
    hidden: { y: 20, opacity: 0 },
    visible: {
      y: 0,
      opacity: 1,
    },
  };

  return (
    <section
      className={
        coors.y <= 590
          ? 'section_profile'
          : 'section_profile section_profile--full'
      }
    >
      {loading ? (
        <div className="div_spinner">
          <Spinner />
        </div>
      ) : (
        <article
          className={
            coors.y <= 590
              ? 'article_profile'
              : ' article_profile article_profile--full'
          }
        >
          {coors.y <= 590 ? null : (
            <i
              className="bx bx-arrow-back"
              onClick={() => {
                setCoors({ y: 590 });
              }}
            ></i>
          )}
          <div className="div_profile">
            <img src={avatar} alt={name} className="img_profile" />
            <h1>@{name}</h1>
            <p>{profession}</p>
            <motion.ul
              className={
                coors.y <= 590
                  ? 'ul_link_container'
                  : 'ul_link_container ul_link_container--full'
              }
              variants={variants}
              initial="hidden"
              animate="show"
            >
              {social.map(({ name, url }) => (
                <LinkItem name={name} url={url} key={url} />
              ))}
              {coors.y >= 700 ? (
                <motion.ul
                  className={coors.y <= 590 ? '' : 'ul_profile_media'}
                  variants={container}
                  initial="hidden"
                  animate="visible"
                >
                  {social.map(({ name, url }) => (
                    <motion.li
                      key={name + url}
                      className="li_profile_media"
                      target="_blank"
                      rel="noreferrer"
                      variants={item}
                    >
                      <a href={url}>
                        <i
                          className={`bx bxl-${name.toLowerCase()} i_link_profile`}
                        ></i>
                      </a>
                    </motion.li>
                  ))}
                </motion.ul>
              ) : null}
            </motion.ul>
          </div>
          <Footer />
        </article>
      )}
      {coors.y <= 590 && (
        <motion.h3
          className="h3_profileMessage"
          animate={{ rotate: [3, 0, 3] }}
          transition={{ repeat: Infinity, repeatType: 'reverse' }}
        >
          Hold and pull up the white bottom line!
        </motion.h3>
      )}
      {coors.y >= 590 && (
        <a
          href="https://github.com/Nicolas-alt/linktr-clone"
          target="_blank"
          rel="noreferrer"
          className="a_profileRepo"
        >
          <i className="bx bxl-github"></i>
        </a>
      )}
    </section>
  );
};

export default Profile;
