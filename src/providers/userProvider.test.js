import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import UserProvider from './UserProvider';

describe('Test on <UserProvider />', () => {
  test('Should render a child ', () => {
    const { container } = render(
      <UserProvider>
        <p>test</p>
      </UserProvider>
    );
    expect(container.firstChild).toMatchInlineSnapshot(`
    <p>
      test
    </p>
  `);
  });
});
