import toast from "react-hot-toast";
const baseUrl = process.env.REACT_APP_API_URL;

export const getData = async () => {
  try {
    const response = await fetch(baseUrl);
    return response.json();
  } catch (error) {
    toast.error('Fail to get data :(', {
      position: 'bottom-right',
    });
  }
};
