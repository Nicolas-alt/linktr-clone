import { Toaster } from 'react-hot-toast';
import Profile from '../../pages/Profile/Profile';
import UserProvider from '../../providers/UserProvider';

function App() {
  return (
    <UserProvider>
      <Profile />
      <Toaster />
    </UserProvider>
  );
}

export default App;
