import React, { useContext } from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';
import { motion } from 'framer-motion';
import { UserContext } from '../../providers/UserProvider';
import './footer.css';

const Footer = () => {
  const { coors, setCoors } = useContext(UserContext);
  return (
    <footer className="footer">
      <SvgIcon />
      {coors.y <= 595 && (
        <motion.div
          className="div_lineBottom"
          drag
          dragConstraints={{ top: 0, right: 0, bottom: 0, left: 0 }}
          dragElastic={1}
          onDragEnd={(event, info) => setCoors({ y: info.point.y * 9 })}
        ></motion.div>
      )}
    </footer>
  );
};

export default Footer;
