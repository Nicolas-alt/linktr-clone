import React, { createContext, useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import { getData } from '../helpers/getData';

export const UserContext = createContext();

const UserProvider = ({ children }) => {
  const [userData, setUserData] = useState({ data: {}, loading: true });
  const [coors, setCoors] = useState({ y: 0 });

  useEffect(() => {
    getData()
      .then((response) => setUserData({ data: response, loading: false }))
      .catch((error) =>
        toast.error(`${error}`, {
          position: 'bottom-right',
        })
      );
  }, []);

  return (
    <UserContext.Provider value={{ userData, coors, setCoors }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
