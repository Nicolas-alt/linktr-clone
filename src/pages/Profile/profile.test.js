import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import Profile from '../../pages/Profile/Profile';
import TestUtlis from '../../components/TestUtils/Test-utils';

describe('Test on <Profile />', () => {
  test('Should take a snapshot ', () => {
    const { container } = render(
      <TestUtlis>
        <Profile />
      </TestUtlis>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
